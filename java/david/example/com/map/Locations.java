package david.example.com.map;

import android.location.Location;

/**
 * Created by David on 2015-10-21.
 */
public class Locations {
    private String name, longitude, latitude;

    public Locations(String name, String longitude, String latitude){
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getlongitude(){
        return longitude;
    }

    public String getLatitude(){
        return latitude;
    }

    public String getName(){
        return name;
    }
}
