package david.example.com.map;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    private Button btnConnect;
    private EditText etName;
    private Controller controller;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initialize(view);
        buttonListener();
        return view;
    }

    private void initialize(View view) {
        btnConnect = (Button)view.findViewById(R.id.btnConnect);
        etName = (EditText)view.findViewById(R.id.etName);
    }

    private void buttonListener() {
        ButtonListener bl = new ButtonListener();
        btnConnect.setOnClickListener(bl);
    }


    public void setController(Controller controller) {
        this.controller = controller;
    }


    private class ButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            controller.connectToServer(etName.getText().toString());
        }
    }
}
