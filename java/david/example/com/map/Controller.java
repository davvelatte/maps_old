package david.example.com.map;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


/**
 * Created by David on 2015-10-14.
 */
public class Controller {
    private MapsActivity mainActivity;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private GoogleMap map;
    private StartConnection startConnection;
    private String myName;
    private JsonStrings jsonStrings;
    private MainFragment mainFragment;
    private Boolean notReady = false;

    public Controller(MapsActivity mainActivity) {
        this.mainActivity = mainActivity;
        startMap();
        startLoginFragment();
    }

    public void connectToServer(String name){
        this.myName = name;
        jsonStrings = new JsonStrings();
        startConnection = new StartConnection("195.178.232.7", 7117, this);
        startConnection.startConnect();
        startMainFragment();
        notReady = true;

    }

    public void disconnect(){
        startConnection.disconnect();
        System.out.println("Ej ansluten till server");
        startLoginFragment();
    }

    public void sendSomeThing(String whatToHappen){
        startConnection.sendSomeThing(whatToHappen);
    }

    private void startLoginFragment(){
        FragmentManager fm = mainActivity.getFragmentManager();
        LoginFragment loginFragment = new LoginFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.res, (Fragment) loginFragment);
        ft.commit();
        loginFragment.setController(this);
    }

    private void startMainFragment() {
        FragmentManager fm = mainActivity.getFragmentManager();
        mainFragment = new MainFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.res, (Fragment) mainFragment);
        ft.commit();
        mainFragment.setController(this);
        mainFragment.setJsonStrings(jsonStrings);
        mainFragment.setMyName(myName);
        startConnection.sendSomeThing(jsonStrings.liveGroups());

    }

    private void startMap() {
        SupportMapFragment supportmapFragment = (SupportMapFragment) mainActivity.getSupportFragmentManager()
                .findFragmentById(R.id.map);
       supportmapFragment.getMapAsync(new OMRC());
        locationManager = (LocationManager) mainActivity.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocList();
    }

    public void leaveGroup() {
        sendSomeThing(jsonStrings.leaveGroup(startConnection.getId()));

    }

    private class OMRC implements OnMapReadyCallback {
        public void onMapReady(GoogleMap googleMap) {
            map = googleMap;
        }
    }

    public void clearMarkers() {
        map.clear();
        locationManager.removeUpdates(locationListener);
    }

    public void addEveryOne(final ArrayList<Locations> list){

        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                map.clear();
                for (int i = 0; i < list.size(); i++) {
                    double latitude = Double.parseDouble(list.get(i).getLatitude());
                    double longitude = Double.parseDouble(list.get(i).getlongitude());
                    LatLng latLng = new LatLng(latitude, longitude);

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, locationListener);
                    map.addMarker(new MarkerOptions().position(latLng).title(list.get(i).getName()));
                }
            }
        });

    }

    private class LocList implements LocationListener {
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            if(notReady) {
                String longi = Double.toString(longitude);
                String lat = Double.toString(latitude);
                sendSomeThing(jsonStrings.myPosition(startConnection.getId(), longi, lat));
                sendSomeThing(jsonStrings.liveGroups());
                Log.d("onLocChanged", "Lng=" + longitude + ",Lat=" + latitude);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
         }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
         }
    }

    public void updateGroups(final ArrayList<String> list){
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainFragment.setAdapter(list);
            }
        });
    }
}
