package david.example.com.map;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    private Button leavGroup, btnAddGroup, btnDisc;
    private EditText etGroupName;
    private Spinner spinnerGroup;
    private Controller controller;
    private JsonStrings jsonStrings;
   // private SpinnerAdapter adapter;
    private String spinnerValue;
    private String name;

    public MainFragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view  = inflater.inflate(R.layout.fragment_main, container, false);
        initialize(view);
        listeners();
        return view;
    }

    private void listeners() {
        ButtonListener bl = new ButtonListener();
        btnAddGroup.setOnClickListener(bl);
        leavGroup.setOnClickListener(bl);
        btnDisc.setOnClickListener(bl);
    }

    private void initialize(View view) {
        btnAddGroup = (Button)view.findViewById(R.id.btnAddGroup);
        leavGroup = (Button)view.findViewById(R.id.btnLeave);
        spinnerGroup = (Spinner)view.findViewById(R.id.spGroup);
        btnDisc = (Button)view.findViewById(R.id.btnDisc);
        etGroupName = (EditText)view.findViewById(R.id.etGroupName);

    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void setJsonStrings(JsonStrings jsonStrings) {
        this.jsonStrings = jsonStrings;
    }

    public void setAdapter(ArrayList<String> list) {
        spinnerGroup.setAdapter(null);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                list);
        spinnerGroup.setAdapter(adapter);
    }

    private void setSpinnerValue(){
        this.spinnerValue = spinnerGroup.getSelectedItem().toString();
    }

    public String getSpinnerValue(){
        return spinnerValue;
    }

    public void setMyName(String myName) {
        this.name = myName;
    }

    private class ButtonListener implements View.OnClickListener {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnAddGroup : controller.sendSomeThing(jsonStrings.addGroup(etGroupName.getText().toString(),name));
                    controller.sendSomeThing(jsonStrings.liveGroups());
                    break;
                case R.id.btnLeave : controller.leaveGroup();
                    controller.clearMarkers();
                    break;
                case R.id.btnDisc : controller.disconnect(); controller.clearMarkers();
                    break;
            }
        }
    }
}
