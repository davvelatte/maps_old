package david.example.com.map;

import android.util.JsonWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedList;

/**
 * Created by David on 2015-10-16.
 */
public class JsonStrings {

    //Metod som skickar ett meddelande till server att jag vill gå med i en grupp.
    public String addGroup(String groupName, String name){
        StringWriter stringWriter = new StringWriter();
        JsonWriter writer = new JsonWriter(stringWriter);
        try {
            writer.beginObject().name("type").value("register").name("group").value(groupName).name("member").value(name).endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    //Metod som skickar meddelande till servern jag vill lämna gruppen med mitt id.
    public String leaveGroup(String id){
        StringWriter stringWriter = new StringWriter();
        JsonWriter writer = new JsonWriter(stringWriter);
        try {
            writer.beginObject().name("type").value("unregister").name("id").value(id).endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    //Metod som kollar vilka medlemmar som är i en grupp.
    public String checkMembers(String groupName){
        StringWriter stringWriter = new StringWriter();
        JsonWriter writer = new JsonWriter(stringWriter);
        try {
            writer.beginObject().name("type").value("members").name("group").value(groupName).endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    //Kollar vilka grupper som finns.
    public String liveGroups(){
        StringWriter stringWriter = new StringWriter();
        JsonWriter writer = new JsonWriter(stringWriter);
        try {
            writer.beginObject().name("type").value("groups").endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    //Skickar min possition.
    public String myPosition(String id, String longitude,String latitude ){
        StringWriter stringWriter = new StringWriter();
        JsonWriter writer = new JsonWriter(stringWriter);
        try {
            writer.beginObject().name("type").value("location").name("id").value(id).name("longitude").value(longitude)
                    .name("latitude").value(latitude).endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }
}
