package david.example.com.map;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;

import java.io.DataOutputStream;
import java.io.IOException;

import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;

import android.os.Handler;


/**
 * Created by David on 2015-10-14.
 */
public class StartConnection{

    //Uppkoppling mot server
    private DataInputStream input;
    private DataOutputStream output;
    private String ip;
    private int connectionPort;
    private InetAddress address;
    private Socket socket;

    //Trådar / klasser
    private Receive receive;
    private RunOnThread thread;
    private Controller controller;

    //Annat
    private String messageFromServer, id;


    public StartConnection(String ip, int connectionPort, Controller controller) {
        this.ip = ip;
        this.connectionPort = connectionPort;
        this.controller = controller;
        thread = new RunOnThread();
    }

    public void startConnect(){
        thread.start();
        thread.execute(new Connect());
    }

    public void sendSomeThing(String message){
        thread.execute(new Send(message));
    }

    public void disconnect(){
        thread.execute(new Disconnect());
    }

    //ANSLUTER TILL SERVERN
    private class Connect implements Runnable{
        public void run() {
            try{
                address = InetAddress.getByName(ip);
                socket = new Socket(address, connectionPort);
                input = new DataInputStream(socket.getInputStream());
                output = new DataOutputStream(socket.getOutputStream());
                output.flush();
                receive = new Receive();
                receive.start();

                System.out.println("ANSLUTEN TILL SERVER ");
            }catch (Exception e){

            }
        }
    }

    //Tar emot meddelanden från servern.
    private class Receive extends Thread {
        public void run() {
            try {
                while (receive != null) {
                    messageFromServer = (String) input.readUTF();
                    System.out.println("Får något från servern " + messageFromServer);
                    if (messageFromServer.contains("register")) {
                        setId(messageFromServer);
                    }
                    if(messageFromServer.contains("groups")) {
                        readJson(messageFromServer);
                    }
                    if(messageFromServer.contains("locations")) {
                        setPossitions(messageFromServer);
                    }

                }
            } catch (Exception e) {
                receive = null;
            }
        }
    }

    private class Send implements Runnable {
        private String messageToSend;
        public Send(String message){
            this.messageToSend = message;
        }
        public void run() {
            try {
                JSONObject json = new JSONObject(messageToSend);
                output.writeUTF(String.valueOf(json));
                output.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class Disconnect implements Runnable {
        public void run() {
            try {
                if (socket != null)
                    socket.close();
                if (input != null)
                    input.close();
                if (output != null)
                    output.close();
                thread.stop();

            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setPossitions(String string){
        String name, longitude, latitude;
        ArrayList<Locations> pos = new ArrayList<>();
        Locations locations;
        try {
            JSONObject transaction;
            JSONObject json = new JSONObject(string);
            JSONArray array = json.getJSONArray("location");
            for(int i=0; i<array.length(); i++){
                transaction = array.getJSONObject(i);
                name = transaction.getString("member");
                longitude = transaction.getString("longitude");
                latitude = transaction.getString("latitude");
                locations = new Locations(name,longitude,latitude);
                pos.add(locations);
                controller.addEveryOne(pos);

            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setId(String str){
        try{
            JSONObject jsonObject = new JSONObject(str);
            id = jsonObject.getString("id");

        }catch (JSONException e){
        }
    }

    public String getId(){
        return id;
    }

    public void readJson(String string)  {
        ArrayList<String> groupList = new ArrayList<>();
        String groups = "";
        try {
            JSONObject transaction;
            JSONObject json = new JSONObject(string);
            JSONArray array = json.getJSONArray("groups");
            for(int i=0; i<array.length(); i++){
                transaction = array.getJSONObject(i);
                groups = transaction.getString("group");
                groupList.add(groups);
                controller.updateGroups(groupList);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

}
